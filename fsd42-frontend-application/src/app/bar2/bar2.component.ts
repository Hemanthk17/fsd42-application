import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bar2',
  templateUrl: './bar2.component.html',
  styleUrls: ['./bar2.component.css']
})
export class Bar2Component implements OnInit  {



  constructor(private list:ProductsService,private router:Router){

    

  }


  ngOnInit(){
      
  }

  
  men(){
    localStorage.setItem("user2","Male");
    this.router.navigate(['Product']);
   }
 
   women(){
     localStorage.setItem("user2","Female");
    this.router.navigate(['Product']);
   }
 
   allProducts(){
     localStorage.setItem("user2","all");
     this.router.navigate(['Product']);
    
   }

  

}
