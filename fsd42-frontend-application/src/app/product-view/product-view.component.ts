import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {

  products: any[] = [];
  requiredp :any; 

  productId :any;

  constructor(private list:ProductsService){

  }

  ngOnInit() {
    this.list.getAllProducts().subscribe(
      (data: any) => {
        this.products = data;
    
        console.log('Data fetched successfully:', this.products);
    
        this.productId = localStorage.getItem("prId"); // ID of the product you want to retrieve
        console.log(this.productId);
    
        this.requiredp = this.products.find((item) => item.id === parseInt(this.productId));
        if (this.requiredp) {
          console.log('Required product:', this.requiredp);
        } else {
          console.log('Product not found.');
        }
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
    
  }
  

  wishlist(){

    const id = localStorage.getItem("userId");
    
    this.list.addtoWishList( this.requiredp.id,id);

    
    this.list.addtoWishList(this.requiredp.id, id)
    .subscribe(response => {
      console.log('Product added to wishlist:', response);
    }, error => {
      console.error('Failed to add product to Wishlist:', error);
    });

   
  }

  cart(){

    const id = localStorage.getItem("userId");
    // this.list.addToCart( this.requiredp.id,id);

    this.list.addToCart(this.requiredp.id, id)
    .subscribe(response => {
      console.log('Product added to cart:', response);
    }, error => {
      console.error('Failed to add product to cart:', error);
    });


  }  
  
  
  

      

  

   

}
