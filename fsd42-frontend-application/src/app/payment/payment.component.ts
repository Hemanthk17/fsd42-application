import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

declare var Razorpay: any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  title = 'Thrift Haven';
  paymentId: string="";
  error: string="";
  form: any = {};
  total:any;
  order_id:any;

  user:any;

  name: any;
  email: any;
  amount: any;




  constructor(private http: HttpClient,private route:ActivatedRoute,private router:Router){
    this.total=this.route.snapshot.paramMap.get('total');

    this.user = { id: '', name: '', dateOfBirth: '', registeredDate: '', gender: '', email: '', password: '' }; 
  }


  ngOnInit() {
    this.total=this.route.snapshot.paramMap.get('total');
    if(this.total < 100){
      this.total = 200;
    }
    
    }
    payNow() {
      this.user = localStorage.getItem("user");
      
      const RazorpayOptions = {
        description: 'Sample Razorpay demo',
        currency: 'INR',
        amount: this.total * 100,
        name: 'ThriftHaven',
        key: 'rzp_test_nWZxyq10ewkHpw',
        image: 'src/assets/image/logo.jpg',
        prefill: {
          name: this.user.name,
          email: this.user.email,
          phone: this.user.phoneNumber
        },
        theme: {
          color: '#d6b9b0'
        },
        modal: {
          ondismiss: () => {
            console.log('Payment dismissed');
          },
          onpaymenterror: (error: any) => {
            console.log('Payment error:', error);
            // Redirect to the failure page
            this.router.navigate(['profile']);
          },
          onpaymentsuccess: (paymentid: any) => {
            console.log('Payment success:', paymentid);
            // Redirect to the success page
            this.router.navigate(['welcome']);
          }
        }
      };
  
      Razorpay.open(RazorpayOptions);
    }


}

