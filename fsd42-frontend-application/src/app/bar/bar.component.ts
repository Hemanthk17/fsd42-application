import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.css']
})
export class BarComponent {
  isLoggedIn:boolean;

  constructor(private service: UserService,private router:Router){
    this.isLoggedIn = this.service.getLoginStatus();
    // console.log(this.isLoggedIn);
  }

  LoginClick(){
    this.router.navigate(['login']);
  }
  LogoutClick(){
    localStorage.clear();
    this.service.setUserLoggedOut();
    this.router.navigate(['welcome']);
  }

  cartClick(){
    this.router.navigate(['cart']);
  }

  wishlistClick(){
    this.router.navigate(['wishlist']);
  }

  orderrClick(){
    this.router.navigate(['order']);
  }



}
