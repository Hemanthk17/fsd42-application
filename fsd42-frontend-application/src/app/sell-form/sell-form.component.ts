import { Component } from '@angular/core';

@Component({
  selector: 'app-sell-form',
  templateUrl: './sell-form.component.html',
  styleUrls: ['./sell-form.component.css']
})
export class SellFormComponent {
  selectedImagePath: string | undefined;

  handleImageSelect(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    if (inputElement.files && inputElement.files.length > 0) {
      const file = inputElement.files[0];
      this.selectedImagePath = URL.createObjectURL(file);
      console.log(this.selectedImagePath);
    }
  }

}
