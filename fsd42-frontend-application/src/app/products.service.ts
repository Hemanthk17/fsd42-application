import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private  baseUrl = 'http://localhost:8080';
 

  constructor(private http: HttpClient) { }


  getAllProducts(): Observable<any> {
    return this.http.get<any>('http://localhost:8080/getAllProducts');
  }

  getAllMenProducts(): Observable<any> {
    return this.http.get<any>('http://localhost:8080/getAllMenProducts');
  }

  getAllWomenProducts() : Observable<any> {
    return this.http.get<any>('http://localhost:8080/getAllWomenProducts');
  }

  getUserWishlist(userId:any): Observable<any>{
    return this.http.get<any>('http://localhost:8080/getUserWishList/'+userId);
  }  

  addToCart(productId: any, userId: any) {
    return this.http.post(`/addToCart/${userId}/${productId}`, {});
  }

  addtoWishList(productId: any, userId: any){
    //addToWishList/{userId}/{productId}
    return this.http.post(`/addToWishList/${userId}/${productId}`, {});
  }

  getUserCart(userId:any): Observable<any>{
    return this.http.get<any>('http://localhost:8080/getUserCart/'+userId);

  }

  deleteUserCartProduct(userId: any, productId: any): Observable<any> {
    const url = `${this.baseUrl}/deleteUserCartProduct/${userId}/${productId}`;
    return this.http.delete(url, { responseType: 'text' });
  }

  deleteUserCartSingleProduct(userId: any, productId: any): Observable<any> {
    const url = `${this.baseUrl}/deleteUserCartSingleProduct/${userId}/${productId}`;
    return this.http.delete(url, { responseType: 'text' });
  }
  

   
  deleteUserWishListProduct(userId: number, productId: number): Observable<string> {
    const url = `${this.baseUrl}/deleteUserWishList/${userId}/${productId}`;
    return this.http.delete(url, { responseType: 'text' });
  }





 



 

}
