import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ProductsService } from '../products.service';
import { Location } from '@angular/common';
import { HotToastModule} from '@ngneat/hot-toast';
import { HotToastService } from '@ngneat/hot-toast';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {

  products:any;

  isLoggedIn: boolean;

  userId: any;

  
  // WishList Empty status

  isEmpty:boolean;

  ngOnInit(){
   
  }

  constructor(private service:UserService,private router:Router,private list:ProductsService,private location: Location,private toast: HotToastService){

    // To check the login status
    this.isLoggedIn = true;
    this.service.getLoginStatus();

    // to get the userid of user
    this.userId =localStorage.getItem("userId");

    //initialize wihlist as empty
    this.isEmpty = true;

    // products from wishlist of user and ot sort in unique
    this.list.getUserWishlist(this.userId).subscribe(
      (data: any[]) => {
        this.products = this.getUniqueProducts(data);
        console.log('Data fetched successfully:', this.products);
    
        // Check if products are fetched successfully
        if (this.products && this.products.length > 0) {
          // Condition if products are fetched
          this.isEmpty = false;
          console.log('Products are fetched');
        } 
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
    

    
  }

  // Helper method to get unique products
    private getUniqueProducts(data: any[]): any[] {
      const uniqueProducts: any[] = [];
      const productIds: Set<number> = new Set();

      for (const product of data) {
        if (!productIds.has(product.id)) {
          productIds.add(product.id);
          uniqueProducts.push(product);
        }
      }

      return uniqueProducts;
    }

  //delete product from wishlist
  deleteWishlistProduct(product: any) {
    console.log(product);
    this.list.deleteUserWishListProduct(this.userId, product.id).subscribe(
      response => {
        console.log(response); // Log the response
        // Refresh the page
        // this.location.go(this.location.path());
        // location.reload();


        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
          this.router.navigate(['wishlist']);
        });
      },
      error => {
        console.error(error); // Handle and log the error
      }
    );
  }

  
  addToCart(product:any){

    
      this.list.addToCart(product.id,this.userId).subscribe(
        response => {
          console.log(response); // Log the response

                      // Set the addedToCart property of the product to true
            product.addedToCart = true;

            // Wait for 7 seconds and reset the addedToCart property to false
            setTimeout(() => {
              product.addedToCart = false;
            }, 7000);
        },
        error => {
          console.error(error); // Handle and log the error
        }
      );
  }

  shopClick(){
    this.router.navigate(['Product']);
  }

}
