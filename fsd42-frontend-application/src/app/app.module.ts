import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { LogoutComponent } from './logout/logout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AdminComponent } from './admin/admin.component';
import { EncrdecrService } from '../app/encrdecr.service';
import { WelcomeComponent } from './welcome/welcome.component';
import { HotToastModule } from '@ngneat/hot-toast';
import { NgxCaptchaModule } from 'ngx-captcha';
import { ProductslistComponent } from './productslist/productslist.component';
import { CartComponent } from './cart/cart.component';
import { BarComponent } from './bar/bar.component';
import { PaymentComponent } from './payment/payment.component';
import { ProfileComponent } from './profile/profile.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { SellFormComponent } from './sell-form/sell-form.component';
import { OrderTrackComponent } from './order-track/order-track.component';
import { Bar2Component } from './bar2/bar2.component';
import { GoogleLoginProvider, GoogleSigninButtonModule, SocialAuthServiceConfig, SocialLoginModule } from '@abacritt/angularx-social-login';
import { Location } from '@angular/common';
import { ProductViewComponent } from './product-view/product-view.component';







@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    LogoutComponent,
    HeaderComponent,
    FooterComponent,
    AdminComponent,
    WelcomeComponent,
    ProductslistComponent,
    CartComponent,
    BarComponent,
    PaymentComponent,
    ProfileComponent,
    WishlistComponent,
    SellFormComponent,
    OrderTrackComponent,
    Bar2Component,
    ProductViewComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    HotToastModule.forRoot(),
    NgxCaptchaModule,
    SocialLoginModule,
    GoogleSigninButtonModule,

  ],
  providers: [  [Location],{
    provide: 'SocialAuthServiceConfig',
    useValue: {
      autoLogin: false,
      providers: [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider(
            '364600431908-3u8ro7jslj5rllrmdncv62638r29os21.apps.googleusercontent.com'
          )
        },

        // {
        //   id: FacebookLoginProvider.PROVIDER_ID,
        //   provider: new FacebookLoginProvider('1217699282282847')
        // }

      ],
      onError: (err) => {
        console.error(err);
      }
    } as SocialAuthServiceConfig,
  }
,EncrdecrService],
  bootstrap: [AppComponent]
})
export class AppModule { }
