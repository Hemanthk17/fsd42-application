import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { EncrdecrService } from '../encrdecr.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HotToastService } from '@ngneat/hot-toast';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  user: any;
  regForm: FormGroup
  siteKey:string="6LdO7iwmAAAAAE9dcJCkvcSZXKCoCYGTE9-qe7uS";
  captchaResponse:any;
  protected aFormGroup: any;


  constructor(private service: UserService, private router: Router, private encr: EncrdecrService, private formBuider: FormBuilder,private toast: HotToastService,private toast2: HotToastService,
    private toast3: HotToastService) {
   
    this.user = { id: '', name: '', dateOfBirth: '', registeredDate: '', gender: '', email: '', password: '' };
    this.regForm = this.formBuider.group({
      firstName:       ['', Validators.required],
      lastName:        ['',Validators.required],
      email:           ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+[a-zA-Z]{1,}\\.[cC][oO][mM]$')]],
      // email:        ['',Validators.required,Validators.pattern("^[a-zA-Z0-9._-]{2,}@[a-zA-Z0-9.-]+\\.com$")],
    
      PhoneNumber:     ['',[Validators.required,Validators.pattern("^[6-9]\\d{9}$")]],
      password:        ['',[Validators.required,Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)]],
      confirmPassword: ['',[Validators.required,Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)]],
      recaptcha:       ['', Validators.required]
    })

    this.toast2.defaultConfig = {
      ...this.toast2.defaultConfig,
      reverseOrder: false
    };

  }

  ngOnInit() {
    // this.toast.loading(" *  are required fields for registartion ")

    const ref = this.toast.show(
      '<span class ="text-danger">*</span>  are required fields for registartion ',
      { autoClose: false, }
    );
    setTimeout(() => {
      ref.close();
    }, 2800);
  }

  register() {
    console.log(this.regForm.value);
    this.user = { id: '', name: '', dateOfBirth: '', registeredDate: '', gender: '', email: '', password: '', phoneNumber: '' };

    // if (this.regForm.invalid) {
    //   return;
    // }

    const currentDate = new Date();
    let check = 1;
    const hashedPassword     = this.encr.encryptPassword(this.regForm.value.password);
    // this.user.id             = this.regForm.value.empId;
    this.user.name           = this.regForm.value.firstName + ' ' + this.regForm.value.lastName;
    // this.user.dateOfBirth    = this.regForm.value.dob;
    this.user.registeredDate = currentDate;
    // this.user.gender         = this.regForm.value.gender;
    this.user.email          = this.regForm.value.email;
    this.user.password       = hashedPassword;
    this.user.phoneNumber    = this.regForm.value.PhoneNumber;
    // Perform further logic with the user object

    //
    if(!this.user.email || this.user.email.trim() === '' ){
      const ref = this.toast.error(
        'Email is not filled.',
        { autoClose: false, icon: '🕵️' }
      );
      setTimeout(() => {
        ref.close();
      }, 2000);

              if(!this.user.phoneNumber || this.user.phoneNumber === ''){
                const ref = this.toast2.error(
                  'Phone Number is not filled.',
                  { autoClose: false, icon: '🔢' }
                );
                setTimeout(() => {
                  ref.close();
                }, 2000);

                      if( !  this.regForm.value.password || this.regForm.value.password.trim() === ''){
                        const ref = this.toast2.error(
                          'password Required',
                          { autoClose: false, icon: '🔒' }
                        );
                        setTimeout(() => {
                          ref.close();
                        }, 2000);
                        

                                if(!this.regForm.value.confirmPassword || this.regForm.value.confirmPassword === ''){
                                  const ref = this.toast2.error(
                                    'Confirm password Required',
                                    { autoClose: false, icon: '🔒' }
                                  );
                                  setTimeout(() => {
                                    ref.close();
                                  }, 2000);
                                }
                       }
               }

    }else if (!this.user.phoneNumber || this.user.phoneNumber === ''){
      const ref = this.toast2.error(
        'Phone Number is not filled.',
        { autoClose: false, icon: '🔢' }
      );
      setTimeout(() => {
        ref.close();
      }, 2000);
      // this.toast2.error("This didn't work.")

            if( !this.regForm.value.password || this.regForm.value.password.trim() === ''){
              const ref = this.toast2.error(
                'password Required',
                { autoClose: false, icon: '🔒' }
              );
              setTimeout(() => {
                ref.close();
              }, 2000);
              

                      if(!this.regForm.value.confirmPassword || this.regForm.value.confirmPassword === ''){
                        const ref = this.toast2.error(
                          'Confirm password Required',
                          { autoClose: false, icon: '🔒' }
                        );
                        setTimeout(() => {
                          ref.close();
                        }, 2000);
                        
                      }
             }
    }else if(!this.regForm.value.password || this.regForm.value.password.trim() === ''){
      const ref = this.toast2.error(
        'password Required',
        { autoClose: false, icon: '🔒' }
      );
      setTimeout(() => {
        ref.close();
      }, 2000);
      

              if(!this.regForm.value.confirmPassword || this.regForm.value.confirmPassword === ''){
                const ref = this.toast2.error(
                  'Confirm password Required',
                  { autoClose: false, icon: '🔒' }
                );
                setTimeout(() => {
                  ref.close();
                }, 2000);
              }
    }else if(!this.regForm.value.confirmPassword || this.regForm.value.confirmPassword === ''){
      const ref = this.toast2.error(
        'Confirm password Required',
        { autoClose: false, icon: '🔒' }
      );
      setTimeout(() => {
        ref.close();
      }, 2000);
    }else{

      if(this.regForm.value.recaptcha.errors?.required ){
        const ref = this.toast2.error(
          'Tick the Captcha',
          { autoClose: false, icon: '🔒' }
        );
        setTimeout(() => {
          ref.close();
        }, 2000);
      }
      else{

              if (this.regForm.get('recaptcha')?.invalid) {
                
                this.service.registerUser(this.user).subscribe((result: any) => {
                  console.log(result);
                // alert("Registration Succesfull");
                this.toast.success('Registration Successfull 🌟🎉✨', {
                            style: {
                              border: '1px solid #713200',
                              padding: '16px',
                              color: '#713200',
                            },
                            iconTheme: {
                              primary: '#713200',
                              secondary: '#FFFAEE',
                            },
                          });
                  this.router.navigate(['login']);
                },
                  (error: any) => {
                    this.toast.warning('Email or Phone Number Already Exists!')
                  });
              } else {
                this.toast2.error("Password Not Matched");
          
              }
    }



      
    //
   
   
    
    
    // 
  }

 }
}
function checkEmail() {
  throw new Error('Function not implemented.');
}

