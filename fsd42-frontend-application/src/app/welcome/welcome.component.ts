import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { UserService } from '../user.service';
import { Router,NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { filter } from 'rxjs/operators';



declare var jQuery: any;


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit{
  isLoggedIn:boolean;

    constructor(private service: UserService,private router: Router,private location: Location){

      this.isLoggedIn = this.service.getLoginStatus();
   
    
    }
  
    ngOnInit() {
        
    }
  
    men(){
      this.router.navigate(['Product']);
      localStorage.setItem("vlaue","Men");
    }

    women(){
      this.router.navigate(['Product']);
      localStorage.setItem("vlaue","Women");
    }

    Cart(){
      this.router.navigate(['cart']);
    }

    LoginClick(){
        this.router.navigate(['login']);
    }  

    handleLogOut(){
      this.service.setUserLoggedOut();
      this.service.setGoogleLoginStatusOut();
      this.router.navigate(['login']);
    }

    logout(){
      localStorage.clear();
      this.service.setUserLoggedOut();
      this.router.navigate(['login']);
    }

    home(){
      this.router.navigate(['welcome']);
    }

    wishList(){
      this.router.navigate(['wishlist'])
    }

    sellForm(){
      this.router.navigate(['sell']);
    }

    buyClick(){
      this.router.navigate(['Product']);
      localStorage.setItem('value',"none");
    }  

    exploreClick(){
     this.router.navigate(['Product']);
     localStorage.setItem('value',"Explore");
    }

    
   showLogin(){
     jQuery('#loginUser').modal('show');
   }

}


