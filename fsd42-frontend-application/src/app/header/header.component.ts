import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { HotToastService } from '@ngneat/hot-toast';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLoggedIn:boolean;
  ngOnInit() {
   
  }
  constructor(private service:UserService,private router:Router,private toast: HotToastService){
    
    this.isLoggedIn = this.service.getLoginStatus();

  }

  handleClick(){
    localStorage.clear();
    this.service.setUserLoggedOut();
    this.router.navigate(['welcome']);
  }

  LoginClick(){
    
    this.router.navigate(['login']);
  }

  CartClick(){
   
    this.router.navigate(['cart']);
  
  }

  WishlistClick(){
   
      this.router.navigate(['wishlist']);  
      
  }


  menClick(){
    this.router.navigate(['Product']);
    localStorage.setItem("vlaue","Men");
  }

  womenClick(){
    this.router.navigate(['Product']);
    localStorage.setItem("vlaue","Women");
  }

  logoClick(){
    this.router.navigate(['welcome']);
  }

  

}
