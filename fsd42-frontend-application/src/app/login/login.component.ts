import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import {  EncrdecrService } from '../encrdecr.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HotToastModule} from '@ngneat/hot-toast';
import { HotToastService } from '@ngneat/hot-toast';
import { GoogleInitOptions, GoogleLoginProvider, SocialAuthService } from '@abacritt/angularx-social-login';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  email: any;
  password: any;
  user: any;
  passwordForm: FormGroup;
  profile : any;

  //google
  private accessToken = '';
  loggedIn: boolean = this.service.getGoogleLoginStatus();
  registerUser :any;


  constructor(private authService: SocialAuthService,private service : UserService,private router : Router, private encr : EncrdecrService,private formBuilder: FormBuilder,private toast: HotToastService){
    this.passwordForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(8), Validators.pattern(/^(?=.[a-z])(?=.[A-Z])(?=.\d)(?=.[@$!%?&])[A-Za-z\d@$!%?&]{8,}$/)]],

      email: ['', [Validators.required, Validators.email]]
    });

    this.registerUser = { id: '', name: '', dateOfBirth: '', registeredDate: '', gender: '', email: '', password: '' };

  }

 


  getAccessToken(): void {
    this.authService.getAccessToken(GoogleLoginProvider.PROVIDER_ID).then(accessToken => this.accessToken = accessToken);
  }

  refreshToken(): void {
    this.authService.refreshAuthToken(GoogleLoginProvider.PROVIDER_ID);
  }
  ngOnInit() {

    if (this.service.getGoogleLoginStatus() == false) {
      this.authService.authState.subscribe((user) => {
        this.user = user;
        const email = user.email;
  
        // Check if the user already exists
        this.service.checkEmailExists(email).then((exists: boolean) => {
          if (exists) {
            // User exists, 
            this.service.googleLogin(email).subscribe(
              (user: any) => {
                this.user = user;
               
                console.log(user);
                localStorage.setItem('userId',user.id);
                localStorage.setItem("Profile", JSON.stringify(this.user));
                localStorage.setItem("user", this.user);

                const ref = this.toast.show(`
                Hello <span class="bg-toast-100">${this.user.name}</span>.`,
                  { autoClose: false, icon: '✅' }
                );
                setTimeout(() => {
                  ref.close();
                }, 1500);
                this.service.setUserLoggedIn();
                this.router.navigate(['welcome']);
                this.service.setGoogleLoginStatusIn();
                
              },
              (error: any) => {
                console.log("Google user retreival failed");
              }
            );

          } else {
            this.registerUser.email = user.email;this.registerUser.name = user.name;
            // User does not exist, register the user
            this.service.registerUser(this.registerUser).subscribe(
              (registrationResponse: any) => {
                // Registration successful, do something with the response
                this.user = user;
                // console.log(user);
                localStorage.setItem('userId',user.id);
                localStorage.setItem("Profile", JSON.stringify(this.user));
                localStorage.setItem("user", this.user);

                const ref = this.toast.show(`
                Hello <span class="bg-toast-100">${this.user.name}</span>.`,
                  { autoClose: false, icon: '✅' }
                );
                setTimeout(() => {
                  ref.close();
                }, 1500);
                this.service.setUserLoggedIn();
                this.router.navigate(['welcome']);
                this.service.setGoogleLoginStatusIn();
              },
              (registrationError: any) => {
                // Registration failed, handle the error
                console.log("Google user registration failed");
              }
            );
          }
        });
        
        this.loggedIn = (user != null);
     
      });
    }
  }
  
 
  async loginSubmit() {

    // if (this.passwordForm.invalid) {
    //   return;
    // }

    const loginForm = this.passwordForm.value;
    console.log(loginForm);

      if(!loginForm.email || loginForm.email === ''){
        this.toast.error("Please Provide Email.");
        if(!loginForm.password || loginForm.password === ''){
          this.toast.error("Please Provide Password.")
        }
      }else if(!loginForm.password || loginForm.password === ''){
        this.toast.error("Please Provide password.")
      }else{
        if (loginForm.email === 'Admin' && loginForm.password === 'hr') {
      
          // Perform other actions for successful login
                const ref = this.toast.show(
                  'Welcome Admin.',
                  { autoClose: false, icon: '✅' }
                );
                setTimeout(() => {
                  ref.close();
                }, 1250);
          
         
          this.service.setUserLoggedIn();
         
        } else {
          const hashedPassword = this.encr.encryptPassword(loginForm.password);
          loginForm.password = hashedPassword;
          console.log(loginForm);
          await this.service.userLogin(loginForm).then((user: any) => {
            this.user = user;
            // console.log(user);
            localStorage.setItem('userId',user.id);
            localStorage.setItem("Profile", JSON.stringify(this.user));
            localStorage.setItem("user", this.user);
          });
          if (this.user != null) {
            // alert("Welcome to " + this.user.name);
            // this.toast.show(`
            // Hello <span class="bg-toast-100">${this.user.name}</span>.`, {
            //   icon: '👏',
            // });
            const ref = this.toast.show(`
            Hello <span class="bg-toast-100">${this.user.name}</span>.`,
              { autoClose: false, icon: '✅' }
            );
            setTimeout(() => {
              ref.close();
            }, 1500);
            this.service.setUserLoggedIn();
            this.router.navigate(['welcome']);
          } else {
            alert('Invalid Credentials');
          }
    
        }
      }
 
  }

  registerClick(){
    this.router.navigate(['register']);
  }

  isLoggedIn(){
   return  this.service.getLoginStatus();
  }
  
  
}