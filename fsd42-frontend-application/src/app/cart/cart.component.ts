import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ProductsService } from '../products.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  products:any;
  total:any;
  isFooter:any;
  uniqueProducts:any;
  totalPrice :any;

  userId:any;

  // Login Status

  isLoggedIn: boolean;

  // cart Empty status

  isEmpty:boolean;


  constructor(private service:UserService,private list:ProductsService,private router:Router,private location: Location){
    this.isLoggedIn = true;
    this.service.getLoginStatus();
    this.isEmpty = true;
  }
  ngOnInit(){

    // localStorage.setItem("Profile", JSON.stringify(this.user));
    //localStorage.setItem("user", this.user);

    const user = localStorage.getItem("user");
    // const userId = localStorage.getItem("userId");
    this.userId = parseInt(localStorage.getItem("userId") || "0");
    this.isFooter = true;

    
    

    this.list.getUserCart(this.userId).subscribe(
      (data: any) => {
        this.products = data;
        console.log('Data fetched successfully:', this.products);
        if (this.products && this.products.length > 0) {
          this.isEmpty = false;
          this.uniqueProducts = Array.from(new Set(this.products.map((product: any) => product.id))).map((id: unknown) => {
            return this.products.find((product: any) => product.id === id);
          });
  
          console.log('Unique products:', this.uniqueProducts);
          
          // Calculate total price
          let totalPrice = 0;
          for (const product of this.uniqueProducts) {
            const subtotal = this.calculateSubtotal(product);
            totalPrice += subtotal;
          }

          this.totalPrice = totalPrice;
          
        }


      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  addToCart(product:any){
    this.list.addToCart(product.id,this.userId).subscribe(
      response => {
        console.log(response); // Log the response
        this.ngOnInit(); // Recall the ngOnInit method

      },
      error => {
        console.error(error); // Handle and log the error
      }
    );
  }


  getProductQuantity(product: any): number {
    // Logic to filter and count the number of products with the same properties
    // You can modify this logic based on your specific requirements
    return this.products.filter((p: any) => p.imageAddress === product.imageAddress && p.price === product.price).length;
  }
  
  calculateSubtotal(product: any): number {
    const quantity = this.getProductQuantity(product);
    return product.price * quantity;
  }


  
    deleteUserCartProduct(product: any) {
      this.list.deleteUserCartProduct(this.userId, product.id).subscribe(
        response => {
          console.log(response); // Log the response
          // Refresh the page 
          // this.location.go(this.location.path());
          // location.reload();

          this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigate(['cart']);
          });
        },
        error => {
          console.error(error); // Handle and log the error
        }
      );
    }

    deleteUserCartSingleProduct(product: any) {
      this.list. deleteUserCartSingleProduct(this.userId, product.id).subscribe(
        response => {
          console.log(response); // Log the response
          // Perform any necessary actions after successful deletion
          this.ngOnInit(); // Recall the ngOnInit method
        },
        error => {
          console.error(error); // Handle and log the error
        }
      );
    }
    

    shopClick(){
      this.router.navigate(['Product']);
    }

    Payment(){
      this.router.navigate(['payment/',this. totalPrice]);
    }

}
