import { NgModule } from '@angular/core';
import {  RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { LogoutComponent } from './logout/logout.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AdminComponent } from './admin/admin.component';
import { HeaderComponent } from './header/header.component';
import { ProductslistComponent } from './productslist/productslist.component';
import { CartComponent } from './cart/cart.component';
import { BarComponent } from './bar/bar.component';
import { ProfileComponent } from './profile/profile.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { SellFormComponent } from './sell-form/sell-form.component';
import { OrderTrackComponent } from './order-track/order-track.component';
import { Bar2Component } from './bar2/bar2.component';
import { PaymentComponent } from './payment/payment.component';
import { ProductViewComponent } from './product-view/product-view.component';



  


const routes: Routes = [
  {path:'',component:WelcomeComponent},
  {path:'login', component:LoginComponent},
  {path:'register', component:RegistrationComponent},
  {path:'logout', component:LogoutComponent},
   {path: 'welcome', component:WelcomeComponent},
   {path:'Admin',component:AdminComponent},
   {path:'header',component:HeaderComponent},
   {path:'Product',component:ProductslistComponent},
   {path:'cart',component:CartComponent},
   {path:'bar',component:BarComponent},
   {path:'profile',component:ProfileComponent },
   {path:'wishlist',component:WishlistComponent},
   {path:'sell',component:SellFormComponent},
   {path:'order',component:OrderTrackComponent},
   {path:'bar2',component:Bar2Component},
   {path:'payment/:total',component:PaymentComponent},
   {path:'productView',component:ProductViewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {


 }
