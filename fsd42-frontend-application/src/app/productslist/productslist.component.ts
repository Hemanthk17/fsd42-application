import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { Router } from '@angular/router';





@Component({
  selector: 'app-productslist',
  templateUrl: './productslist.component.html',
  styleUrls: ['./productslist.component.css']
})
export class ProductslistComponent implements OnInit {

  /* gender,price filters  */

    // showGenderOptions: boolean = false;
    // showCategoryOptions: boolean = false;
    // showPriceOptions: boolean = false;

   /* gender,price filters  */


  products: any[] = [];
  product : any;
  filteredProducts: any[] = []; 

  loadedProducts: any[] = []; // Array to store loaded products
  loadMoreCount = 5; // Number of products to load on each "Load More" click
  isLoading: boolean;

  /* Colour,size  Filter*/

  color: string[] = [];
  size: string[] = [];
  selectedColor: any;
  selectedSize: any;
 
  constructor(private list:ProductsService,private router:Router){
    this.isLoading = false;
  }
  ngOnInit() {

    
    const user2 = localStorage.getItem("user2");
    const category = user2 === "Male" ? "Male" : user2 === "Female" ? "Female" : "all";
  
    this.list.getAllProducts().subscribe(
      (data: any[]) => {
        this.products = data;
  
        if (category === "all") {
          this.filteredProducts = this.products.slice(0, this.loadMoreCount);
        } else {
          this.filteredProducts = this.products.filter((product: any) =>
            product.category === category
           
          );
         
        }
  
        this.loadedProducts = this.filteredProducts;
        console.log('Data fetched successfully:', this.products);
        this.updateSizeAndColor();
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  

    startLoading() {
      this.isLoading = true;
    
      setTimeout(() => {
        // Perform the actual loading action here
        this.loadMoreProducts();
    
        // Reset the loading state after 2.5 seconds
        setTimeout(() => {
          this.isLoading = false;
        }, 1500);
      }, 3000);
    }
  
  loadMoreProducts() {
    const remainingProducts = this.products.slice(this.loadedProducts.length, this.loadedProducts.length + this.loadMoreCount);
    this.loadedProducts = [...this.loadedProducts, ...remainingProducts];
    this.filteredProducts = this.loadedProducts;
    this.updateSizeAndColor();
  }
  
  
  updateSizeAndColor() {
    this.size = [...new Set(this.loadedProducts.map((product: any) => product.productSize) as string[])];
    this.color = [...new Set(this.loadedProducts.map((product: any) => product.productColor) as string[])];
  }
  

  men(){
   localStorage.setItem("user2","Male");
   this.router.navigate(['Product']);
  }

  women(){
    localStorage.setItem("user2","Female");
   this.router.navigate(['Product']);
  }

  allProducts(){
    localStorage.setItem("user2","all");
    this.router.navigate(['Product']);
   
  }

  addToCart(product: any) {
    const productId = product.id; 
    const userId =   parseInt(localStorage.getItem("userId") || "0");; // Replace with the actual user ID
    console.log(product);
  
    this.list.addToCart(productId, userId)
      .subscribe(response => {
        console.log('Product added to cart:', response);
      }, error => {
        console.error('Failed to add product to cart:', error);
      });
  }
  

  viewCart(){
    const userId  = parseInt(localStorage.getItem("userId") || "0");;
    this.list.getUserCart(userId).subscribe( (data: any) => {
      this.filteredProducts = data;
      console.log('cart fetched successfully:', this.products);
    },
    (error: any) => {
      console.error('Error fetching data:', error);
    });
  }
  

  // Gender,category,price filter 

  // toggleGenderOptions() {
  //   this.showGenderOptions = !this.showGenderOptions;
  // }

  // toggleCategoryOptions() {
  //   this.showCategoryOptions = !this.showCategoryOptions;
  // }
  // togglePriceOptions() {
  //   this.showPriceOptions = !this.showPriceOptions;
  // }

  // Gender,category,price filter 

  

  // Color,Size,price

  showColor(color: any) {
    this.selectedColor = color;
    if (this.selectedSize == null) {
      this.filteredProducts = this.loadedProducts.filter((product: any) =>
        product.productColor === this.selectedColor
      );
      this.color = [...new Set(this.filteredProducts.map((product: any) => product.productColor))];
      this.size = [...new Set(this.filteredProducts.map((product: any) => product.productSize))];
    } else {
      this.filteredProducts = this.loadedProducts.filter((product: any) =>
        product.productColor === this.selectedColor && product.productSize === this.selectedSize
      );
      this.size = [...new Set(this.filteredProducts.map((product: any) => product.productSize))];
      this.color = [...new Set(this.filteredProducts.map((product: any) => product.productColor))];
    }
  }
  
  resetSelectedColor() {
    this.selectedColor = null;
    if (this.selectedSize == null) {
      this.size = [...new Set(this.loadedProducts.map((product: any) => product.productSize))];
      this.color = [...new Set(this.loadedProducts.map((product: any) => product.productColor))];
      this.filteredProducts = this.loadedProducts;
    } else {
      this.filteredProducts = this.loadedProducts.filter((product: any) =>
        product.productSize === this.selectedSize
      );
      this.color = [...new Set(this.filteredProducts.map((product: any) => product.productColor))];
    }
  }
  
  showSize(size: any) {
    this.selectedSize = size;
    if (this.selectedColor == null) {
      this.filteredProducts = this.loadedProducts.filter((product: any) =>
        product.productSize === this.selectedSize
      );
      this.size = [...new Set(this.filteredProducts.map((product: any) => product.productSize))];
      this.color = [...new Set(this.filteredProducts.map((product: any) => product.productColor))];
    } else {
      this.filteredProducts = this.loadedProducts.filter((product: any) =>
        product.productColor === this.selectedColor && product.productSize === this.selectedSize
      );
      this.size = [...new Set(this.filteredProducts.map((product: any) => product.productSize))];
      this.color = [...new Set(this.filteredProducts.map((product: any) => product.productColor))];
    }
  }
  
  resetSelectedSize() {
    this.selectedSize = null;
    if (this.selectedColor == null) {
      this.color = [...new Set(this.loadedProducts.map((product: any) => product.productColor))];
      this.size = [...new Set(this.loadedProducts.map((product: any) => product.productSize))];
      this.filteredProducts = this.loadedProducts;
    } else {
      this.filteredProducts = this.loadedProducts.filter((product: any) =>
        product.productColor === this.selectedColor
      );
      this.size = [...new Set(this.filteredProducts.map((product: any) => product.productSize))];
    }
  }
  
  

  // Color,Size,price



  // product view

  pr(product:any){

    localStorage.setItem("userId","4");
    localStorage.setItem("prId",product.id);
    this.router.navigate(['productView']);

  }



  

}
