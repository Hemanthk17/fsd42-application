import { Component } from '@angular/core';
import { Router } from '@angular/router';

declare var jQuery: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  /*title = 'Thrift haven';*/
  title = 'fsd42-frontend-application';
  constructor(private router: Router) {}

  isLoginOrRegisterComponent(): boolean {
    const currentRoute = this.router.url;
    return currentRoute.includes('/login') || currentRoute.includes('/register') || currentRoute === '/';
  }
  

  // || currentRoute.includes('/')
  
  showLogin(): void {
    jQuery('#login').modal('show');
  }
  
}
