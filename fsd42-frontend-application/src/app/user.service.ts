import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class UserService {

 

  loginStatus: boolean;


  // login
  this: any;
  googleloginStatus: boolean


  constructor( private http: HttpClient) { 
    this.loginStatus = false;
    this.loginStatus = false;
    this.googleloginStatus = false;

  }

  getGoogleLoginStatus(): boolean {
    return this.googleloginStatus;
  }
  setGoogleLoginStatusIn() {
    this.googleloginStatus = true;
  }

  setGoogleLoginStatusOut(){
    this.googleloginStatus = false;
  }


  getAllUsers() : any{
    this.http.get('http://localhost:8080/getAllUsers');
  }

  setUserLoggedIn(): any {
    this.loginStatus = true ;
  }

  getEmails() {
    return this.http.get('getAllEmail');
  }

  setUserLoggedOut(): any{
  this.loginStatus = false;
  } 
  getLoginStatus(): boolean{
   return this.loginStatus;
  }

  userLogin(loginForm: any): any {
  return this.http.get('login/' + loginForm.email + "/" + loginForm.password).toPromise();
  } 

  googleLogin(email:any):any{
    return this.http.get('getUser'+email);
  }

 registerUser(user: any): any{
  return this.http.post('registerUser',user);
 }

 checkEmailExists(email: any): Promise<boolean> {
  return new Promise<boolean>((resolve, reject) => {
    this.http.get('check-email/' + email, { responseType: 'text' })
      .subscribe(
        (response) => {
          if (response === 'true') {
            resolve(true); // Email exists
          
          } else {
            resolve(false); // Email does not exist or API response is null
          }
        },
        (error) => {
          reject(error); // API request failed
        }
      );
  });
}







 
}
