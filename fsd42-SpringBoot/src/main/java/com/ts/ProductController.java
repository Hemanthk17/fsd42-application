package com.ts;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDAO;
import com.model.Product;
import com.model.User;


@RestController
public class ProductController {
	
	@Autowired
	ProductDAO productDAO;
	
	@GetMapping("/getAllProducts")
	public List<Product> getAllProducts(){
		return productDAO.getAllProducts();
	}
	
	@GetMapping("/getAllMenProducts")
	public List<Product> getAllMenProducts(){
		return productDAO.getAllMenProducts();
	}
	
	@GetMapping("/getAllWomenProducts")
	public List<Product> getAllWomenProducts(){
		return productDAO.getAllWomenProducts();
	}
	
	
	@GetMapping("/getProductById/{productId}")
	public Product getproductById(@PathVariable("productId") int productId){
		return productDAO.getProductById(productId);
	}
	
//	@GetMapping("/getProductByName/{ProductName}")
//	public User getUserByName(@PathVariable ("ProductName")String ProductName){
//		return productDAO.getUserByName(ProductName);
//	}
	
	@PostMapping("registerProduct")
	public Product registerProduct(@RequestBody Product product){

		return productDAO.addProduct(product);
	}
	
	@PostMapping("updateProduct")
	public Product updateProduct(@RequestBody Product product){
		return productDAO.updateProduct(product);
	}
	
	@DeleteMapping("deleteProduct/{productId}")
	public String deleteProduct(@PathVariable("productId")int productId){
		productDAO.deleteProductById(productId);
		
		return "User With UserId : "+ productId +" Deleted Succesfully";
	}

}
