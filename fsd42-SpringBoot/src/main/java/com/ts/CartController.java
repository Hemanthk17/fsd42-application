package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CartDAO;
import com.model.Cart;
import com.model.Product;

@RestController
@CrossOrigin(origins="http://localhost:4200")  
public class CartController {
	
	@Autowired
	CartDAO cartDAO;

	@GetMapping("/getUserCart/{userId}")
	public List<Product> getUserCart(@PathVariable int userId){
		return cartDAO.getUserCart(userId);
	}
	
	@DeleteMapping("/deleteUserCart/{userId}")
	public String deleteUserCart(@PathVariable ("userId")int userId){
		cartDAO.deleteUserCart(userId);
		return "Cart with  UserId: "+userId+ " Deleted Successfully!!!";
		}
	
	@PostMapping ("/addToCart/{userId}/{productId}")
	public int addToCart(@PathVariable("userId") int userId,@PathVariable("productId") int productId){
		return cartDAO.addToCart(userId,productId);
		
	}
	
	@DeleteMapping("/deleteUserCartProduct/{userId}/{productId}")
	public String deleteUserCartProduct(@PathVariable ("userId")int userId,@PathVariable("productId")int productId){
		cartDAO.deleteUserCartProduct(userId,productId);
		return "Cart with  UserId: "+userId+ " and ProductId:"+productId+" Deleted Successfully!!!";
		}
	
	@DeleteMapping("/deleteUserCartSingleProduct/{userId}/{productId}")
	public String deleteUserCartSingleProduct(@PathVariable ("userId")int userId,@PathVariable("productId")int productId){
		cartDAO.deleteUserCartSingleProduct(userId,productId);
		return "Cart with  UserId: "+userId+ " and ProductId:"+productId+" Deleted Successfully!!!";
		}
	


}
