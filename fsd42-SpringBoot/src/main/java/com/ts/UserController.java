package com.ts;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDAO;
import com.model.User;

@RestController
public class UserController {
	
	@Autowired
	UserDAO  userDAO;
	
	@GetMapping("/getAllUsers")
	public List<User> getAllUsers(){
		return userDAO.getAllUsers();
	}
	
	@GetMapping("/getUserById/{userId}")
	public User getUserById(@PathVariable("userId") int userId){
		return userDAO.getUserById(userId);
	}
	
	@GetMapping("/login/{emailId}/{password}")
	public User login(@PathVariable ("emailId")String emailId,@PathVariable("password")String password){
		return userDAO.login(emailId, password);
	}
	
	@GetMapping("/getUserByName/{userName}")
	public User getUserByName(@PathVariable ("userName")String userName){
		return userDAO.getUserByName(userName);
	}
	
	@GetMapping("/check-email/{email}")
	public String checkUserByMail(@PathVariable("email")String email){
		return userDAO.checkUserByMail(email);
	}
	
	@GetMapping("/getUser{email}")
	public User getUser(@PathVariable ("email")String email){
		return userDAO.getUser(email);
	}
	
	@PostMapping("registerUser")
	public User registerUser(@RequestBody User user){
		User user2 = null;
		boolean flag2 = true;

		try {
			String EmailID = user.getEmail();
			System.out.println(EmailID);
			String subj = "registration";
			String message = " Dear "+ user.getName()+",\n\n"

						+"Welcome to Thrift Haven!\n\n"+" We're delighted to have you as a member of our thrifting community. At Thrift Haven, we believe in the joy of discovering hidden treasures and finding new life in pre-loved items. Our platform is designed to connect thrifters like you with unique and affordable finds."
							
						+"\n"+" Please click the link below to verify your registration: "
						+"http://localhost:4200/  "+"\n\n"+
							
	                  "Happy thrifting and may you have countless delightful adventures in finding one-of-a-kind gems! We're excited to embark on this thrifting journey with you.\n\n\n"+
							
							"Thank you for choosing ThriftHaven,\n"+
							"Team  ThriftHaven";
			User user1 = new User(user.getName(),user.getDateOfBirth(),user.getRegisteredDate(),user.getGender(),user.getEmail(),user.getPassword(),user.getPhoneNumber());

			user2 = userDAO.registerUser(user1);
			EmailService.sendEmail(message, subj, EmailID, "thrifthaven00@gmail.com");

		} catch (Exception e) {
			e.printStackTrace();
			flag2 = false;

		}
		if (flag2) {
			return user2;
		} else {

			return null;
		}


		
	}
	
	@PostMapping("updateUser")
	public User updateUser(@RequestBody User user){
		return userDAO.updateUser(user);
	}
	
	@DeleteMapping("deleteUser/{userId}")
	public String deleteUser(@PathVariable("userId")int userId){
		userDAO.deleteUser(userId);
		
		return "User With UserId : "+ userId +" Deleted Succesfully";
	}
	
	   
	
	

}
