package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CartDAO;
import com.dao.WishListDAO;
import com.model.Product;

@RestController
@CrossOrigin(origins="http://localhost:4200")  
public class WishListController {

	@Autowired
	WishListDAO wishListDAO;
	

	@GetMapping("/getUserWishList/{userId}")
	public List<Product> getUserWishList(@PathVariable int userId){
		return wishListDAO.getUserCart(userId);
	}
	
	@DeleteMapping("/deleteUserWishList/{userId}")
	public String deleteUserWishList(@PathVariable ("userId")int userId){
		wishListDAO.deleteUserWishlist(userId);
		return "WishList with  UserId: "+userId+ " Deleted Successfully!!!";
		}
	
	@PostMapping ("/addToWishList/{userId}/{productId}")
	public int addWishList(@PathVariable("userId") int userId,@PathVariable("productId") int productId){
		return wishListDAO.addToWishList(userId,productId);	
	}
	
	@DeleteMapping("/deleteUserWishList/{userId}/{productId}")
	public String deleteUserWishListProduct(@PathVariable("userId")int userId,@PathVariable("productId")int productId){
		wishListDAO.deleteUserWishListProduct(userId,productId);
		return "WishList Product with  UserId: "+userId+ " and ProductId:"+productId+" Deleted Successfully!!!";
	}



}
