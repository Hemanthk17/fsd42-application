package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Product;
import com.model.User;

@Repository
public interface ProductRepository extends JpaRepository<Product,Integer>{
	
	@Query("FROM Product p WHERE p.productGender = 'Male'")
	public List<Product> getMaleProducts();

	@Query("FROM Product p WHERE p.productGender = 'Female'")
	public List<Product> getFemaleProducts();


	

}
