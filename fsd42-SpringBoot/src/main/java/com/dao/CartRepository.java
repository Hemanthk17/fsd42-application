package com.dao;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.model.Cart;
import com.model.Product;

@Repository
public interface CartRepository extends JpaRepository<Cart,Integer> {


	@Query("SELECT c FROM Cart c WHERE c.user.id = :userId")
	List<Cart> findCartByUserId(@Param("userId") int userId);
	
	@Query("SELECT c.product FROM Cart c WHERE c.user.id = :userId")
	List<Product> findProductsByUserId(@Param("userId") int userId);


	@Modifying
    @Query("DELETE FROM Cart c WHERE c.user.id = :userId AND c.product.id = :productId ")
	@Transactional
    int deleteUserCartProduct(@Param("userId") int userId, @Param("productId") int productId);
	
	@Modifying
    @Query("DELETE FROM Cart c WHERE c.user.id = :userId")
	@Transactional
    int deleteUserCart(@Param("userId")int userId);
	
	@Modifying
	@Query("DELETE FROM Cart c WHERE c.user.id = :userId AND c.product.id = :productId")
	@Transactional
	int deleteUserCartSingleProduct(@Param("userId") int userId, @Param("productId") int productId);

	
	@Modifying
	@Query("INSERT INTO Cart (product, user) SELECT p, u FROM Product p, User u WHERE p.productId= :productId AND u.id= :userId")
	@Transactional
	int addToCart(@Param("userId") int userId, @Param("productId") int productId);
	


 }




