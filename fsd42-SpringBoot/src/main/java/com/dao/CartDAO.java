package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Cart;
import com.model.Product;

@Service
public class CartDAO {
	
	@Autowired
	CartRepository cartRepository;


	public List<Product> getUserCart(int userId) {
		return cartRepository.findProductsByUserId(userId);
	}

	public int deleteUserCart(int userId) {
		return cartRepository.deleteUserCart(userId);
	}
	
	public int addToCart(int userId,int productId){
		return cartRepository. addToCart(userId,productId);
	}

	public int deleteUserCartProduct(int userId,int productId) {
		return cartRepository.deleteUserCartProduct(userId,productId);
		
	}
	
	public int deleteUserCartSingleProduct(int userId,int productId) {
		return cartRepository.deleteUserCartSingleProduct(userId,productId);
		
	}
	
	
	
	

}
