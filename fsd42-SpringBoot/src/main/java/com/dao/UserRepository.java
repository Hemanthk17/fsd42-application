package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.User;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
	
	@Query("from User u Where u.email = :emailId and u.password = :password")
	public User login(@Param("emailId")String emailId, @Param("password")String password);

	@Query("from User u Where u.name = :userName")
	public User findByName(@Param("userName")String userName);
	

	
	@Query("from User u where u.id = :id")
	public User findUserById(@Param("id")long id);
	
	@Modifying
	@Query("DELETE from User u Where u.id = :id")
	public void deleteUserById(@Param("id")long id);

	
	@Query("SELECT CASE WHEN COUNT(u) > 0 THEN true ELSE false END FROM User u WHERE u.email = :emailId")
	public boolean existsByEmail(@Param("emailId") String email);
	
	@Query("FROM User u WHERE u.email = :email")
	public User findByMail(@Param("email") String email);
	


}
