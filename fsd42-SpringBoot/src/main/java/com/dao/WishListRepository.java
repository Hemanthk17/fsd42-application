package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import com.model.Product;
import com.model.WishList;

@Repository
public interface WishListRepository extends JpaRepository<WishList,Integer> {

	@Query("SELECT w FROM WishList w WHERE w.user.id = :userId")
	List<WishList> findWishListByUserId(@Param("userId") int userId);
	
	
	@Query("SELECT w.product FROM WishList w WHERE w.user.id = :userId")
	List<Product> findProductsByUserId(@Param("userId") int userId);
	
	@Modifying
	@Query("DELETE FROM WishList w WHERE w.user.id = :userId AND w.product.id = :productId ")
	@Transactional
	int deleteUserWishListProduct(@Param("userId") int userId, @Param("productId") int productId);

	@Modifying
    @Query("DELETE FROM WishList w WHERE w.user.id = :userId")
	@Transactional
    int deleteUserWishList(@Param("userId")int userId);
	
	@Modifying
	@Query("INSERT INTO WishList (product, user) SELECT p, u FROM Product p, User u WHERE p.productId= :productId AND u.id= :userId")
	@Transactional
	int addToCart(@Param("userId") int userId, @Param("productId") int productId);


}
