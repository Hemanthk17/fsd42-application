package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.model.Product;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ProductDAO {
    
    @Autowired
    ProductRepository productRepository;

    public List<Product> getAllProducts() {
    	return productRepository.findAll();    }
    
    public List<Product> getAllMenProducts(){
    	return productRepository.getMaleProducts();
    }
    
    public List<Product> getAllWomenProducts(){
    	return productRepository.getFemaleProducts();
    }
   

    public Product getProductById(int productId) {
        return productRepository.findById(productId).orElse(null);
    }

    public Product addProduct(Product product) {
        return productRepository.save(product);
    }

    public Product updateProduct(Product product) {
        return productRepository.save(product);
    }

    public void deleteProduct(Product product) {
        productRepository.delete(product);
    }
    
    public void deleteProductById(int productId){
    	productRepository.deleteById(productId);
    }
}
