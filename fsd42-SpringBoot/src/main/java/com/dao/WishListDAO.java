package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class WishListDAO {
	
	@Autowired
	WishListRepository wishListRepository;
	
	public List<Product> getUserCart(int userId) {
		return wishListRepository.findProductsByUserId(userId);
	}
	

	public int deleteUserWishlist(int userId) {
		return wishListRepository.deleteUserWishList(userId);
	}
	
	public int addToWishList(int userId,int productId){
		return wishListRepository. addToCart(userId,productId);
	}


	public int deleteUserWishListProduct(int userId, int productId) {
		return wishListRepository.deleteUserWishListProduct(userId, productId);
		
	}

}
