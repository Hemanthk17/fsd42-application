package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.User;
import com.ts.EmailService;

@Service
public class UserDAO {
	
	@Autowired
	UserRepository userRepository;
	
	public List<User> getAllUsers(){
		return userRepository.findAll();
	}
	
	public User getUserById(int userId){
		return userRepository.findById(userId).orElse(null);
	}
	
	public User login(String emailId,String password){
		return userRepository.login(emailId,password);
	}
	
	public User getUserByName(String userName){
		return userRepository.findByName(userName);
	}
	
	public User registerUser(User User){
		
		return userRepository.save(User);
	}
	
	public User updateUser(User User){
		return userRepository.save(User);
	}
	
	public void deleteUser(int id){
		userRepository.deleteById(id);
	}

	public User getUser(String email) {
		return userRepository.findByMail(email);
	}

	public String checkUserByMail(String email) {
		return  ""+userRepository.existsByEmail(email);
	}
	
	



	
	

}
