package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;
    private LocalDate dateOfBirth;
    private LocalDate registeredDate;
    private String gender;
    
    @Column(unique = true)
    private String email;
    private String password;
    private boolean isActive = true;
    
    @Column(unique = true)
	private String phoneNumber;
 
    

    // Constructors, getters, setters, and toString

    public User() {
    	super();
    }

    public User(String name, LocalDate dateOfBirth, LocalDate registeredDate, String gender, String email, String password,String PhoneNumber) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.registeredDate = registeredDate;
        this.gender = gender;
        this.email = email;
        this.password = password;
        this.phoneNumber=PhoneNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public LocalDate getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(LocalDate registeredDate) {
        this.registeredDate = registeredDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", dateOfBirth=" + dateOfBirth + ", registeredDate="
				+ registeredDate + ", gender=" + gender + ", email=" + email + ", password=" + password + ", isActive="
				+ isActive + ", phoneNumber=" + phoneNumber + "]";
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	public String getPhoneNumber() {
			return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
	}

	public String getUserName() {
		
		if(name == null )return "Thrifter";
		else return name;
	}
}
