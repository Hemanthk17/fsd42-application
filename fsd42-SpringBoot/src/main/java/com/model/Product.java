package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int productId;
    
    private String productName;
    private String productDescription;
    private double productPrice;
    private String productGender;
    private String productClothingType;
    private String productCategory;
    private String productImageAddress;
    private String productColor;
    private String productSize;

    // Constructors, getters, and setters

	public Product() {
        // Default constructor
    	super();
    }

	public Product(int productId, String productName, String productDescription, double productPrice,
			String productGender, String productClothingType, String productCategory, String productImageAddress,
			String productColor, String productSize) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productDescription = productDescription;
		this.productPrice = productPrice;
		this.productGender = productGender;
		this.productClothingType = productClothingType;
		this.productCategory = productCategory;
		this.productImageAddress = productImageAddress;
		this.productColor = productColor;
		this.productSize = productSize;
	}

    // Getters and Setters

    public int getId() {
        return productId;
    }

    public void setId(int id) {
        this.productId = id;
    }

    public String getName() {
        return productName;
    }

    public void setName(String name) {
        this.productName = name;
    }

    public String getDescription() {
        return productDescription;
    }

    public void setDescription(String description) {
        this.productDescription = description;
    }

    public double getPrice() {
        return productPrice;
    }

    public void setPrice(double price) {
        this.productPrice = price;
    }

    public String getGender() {
        return productGender;
    }

    public void setGender(String gender) {
        this.productGender = gender;
    }

    public String getClothingType() {
        return productClothingType;
    }

    public void setClothingType(String clothingType) {
        this.productClothingType = clothingType;
    }
    
    public String getProductCategory() {
  		return productCategory;
  	}

  	public void setProductCategory(String productCategory) {
  		this.productCategory = productCategory;
  	}
  	
  	 public String getProductImageAddress() {
 		return productImageAddress;
 	}

 	public void setProductImageAddress(String productImageAddress) {
 		this.productImageAddress = productImageAddress;
 	}
 	

	public String getProductColor() {
		return productColor;
	}

	public void setProductColor(String productColor) {
		this.productColor = productColor;
	}

	public String getProductSize() {
		return productSize;
	}

	public void setProductSize(String productSize) {
		this.productSize = productSize;
	}

    
    @Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", productDescription="
				+ productDescription + ", productPrice=" + productPrice + ", productGender=" + productGender
				+ ", productClothingType=" + productClothingType + ", productCategory=" + productCategory
				+ ", productImageAddress=" + productImageAddress + ", productColor=" + productColor + ", productSize="
				+ productSize + "]";
	}

}

