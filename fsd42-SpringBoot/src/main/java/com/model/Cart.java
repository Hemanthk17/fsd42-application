package com.model;
import java.util.Date;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cartId;
    
    @ManyToOne
    @JoinColumn(name = "productId")
    private Product product;
    
    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;
    
    public Cart() {
    	super();
    }

    public Cart(Product product, User user) {
        this.product = product;
        this.user = user;
    }
    
    // Getters and setters
    public int getId() {
        return cartId;
    }

    public void setId(int cartId) {
        this.cartId = cartId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    // toString method
    @Override
    public String toString() {
        return "Cart{" +
                "id=" + cartId +
                ", product=" + product +
                ", user=" + user +
                '}';
    }
}
