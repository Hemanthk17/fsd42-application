package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class WishList {
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private int WishId;
	 
	    @ManyToOne
	    @JoinColumn(name = "productId")
	    private Product product;
	    
	    @ManyToOne
	    @JoinColumn(name = "userId")
	    private User user;
	    
	    public WishList() {
	    	super();
	    }

		public WishList( Product product, User user) {
			this.product = product;
			this.user = user;
		}

		public int getWishId() {
			return WishId;
		}

		public void setWishId(int wishId) {
			WishId = wishId;
		}

		public Product getProduct() {
			return product;
		}

		@Override
		public String toString() {
			return "WishList [WishId=" + WishId + ", product=" + product + ", user=" + user + "]";
		}

		public void setProduct(Product product) {
			this.product = product;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}
	

}
